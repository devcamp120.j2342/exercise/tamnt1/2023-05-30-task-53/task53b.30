import model.Customer;
import model.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "tam1", 10);
        Customer customer2 = new Customer(2, "tam2", 20);
        System.out.println(customer1);
        System.out.println(customer2);
        Invoice invoice1 = new Invoice(1, customer1, 200.00);
        Invoice invoice2 = new Invoice(1, customer2, 400.00);

        System.out.println(invoice1);
        System.out.println(invoice2);
    }
}
